//
//  AppDelegate.h
//  IOS_gesture
//
//  Created by Maculish Ting on 15/5/18.
//  Copyright (c) 2015年 LYD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

