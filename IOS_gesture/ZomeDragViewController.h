//
//  ZomeDragViewController.h
//  IOS_gesture
//
//  Created by Maculish Ting on 15/5/18.
//  Copyright (c) 2015年 LYD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPUserResizableView.h"

@interface ZomeDragViewController : UIViewController<UIGestureRecognizerDelegate, SPUserResizableViewDelegate>
{
    SPUserResizableView *currentlyEditingView;
    SPUserResizableView *lastEditedView;
}

@end
